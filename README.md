# Python project - Data exploration

## 1. How to visualize the project?
To visualize the project on your platform, please follow these steps.

#### Step 1
![Alt text](image.png)

#### Step 2
![Alt text](image-1.png)

## 2. Project Description

The project aimed to conduct data exploration and visualization. In the first part of the study, data was extracted from two Excel files. Subsequently, information about the data in tables was presented. For further exploration, the data was merged. A "Notes” column was removed from the original data, and an auxiliary "Count" column was added to facilitate calculations.

The study explored variables related to:

- Achieved gold medals,
- Gender of Olympians,
- Athletes' ages,
- Weight information,
- Height information.


## 3. Data
The data is sourced from Kaggle, which provides various datasets. The presented data pertains to the Olympic Games that occurred from 1896 to 2016.

- ID – Unique participant ID
- Name - First name
- Sex - M or F
- Age - Age
- Height – Height in cm
- Weight – Weight in kg
- Team – Team name
- NOC - National Olympic Committee
- Games – Year and type of games
- Year – Year of the games
- Season – Summer or Winter Games
- City – Host city of the games
- Sport - Sport discipline
- Event – Event name
- Medal - Medals obtained: Gold, Silver, Bronze, or NA"


