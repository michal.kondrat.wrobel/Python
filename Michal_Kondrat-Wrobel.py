#!/usr/bin/env python
# coding: utf-8


# ## Opis projektu

# Celem projektu było przeprowadzanie eksploracji oraz wizualizacji danych.  W pierwszej części pracy dane zostały zaciągnięte z dwóch plików Excel.  
# Następnie zostały przedstawione informacje o danych z tabel. W celu dalszej eksploracji, dane zostały ze sobą połączone. 
# Z pierwotnych danych została usunięta kolumna ‘Notes’ oraz dodałem pomocniczą kolumnę ‘Count’, która pozwoliła na sprawniejsze obliczenia.
# 
# W pracy eksplorowane były zmienne dotyczące: 
# - uzyskanych złotych medali,
# - płci olimpijczyków 
# - wieku sportców
# - informacji wagi oraz wysokości
# 

# ## Opis danych

# Dane pochodzą ze strony **Kaggle**, która udostępnia różnego rodzaju zbiory danych.
# Przedstawione dane dotyczą igrzysk olimpijskich, które miały miejsca od 1896 roku do 2016 roku. 
# 
# 1.	ID – Unikalny numer id uczestnika
# 2.	Name - Imię
# 3.	Sex - M or F
# 4.	Age - wiek
# 5.	Height – wysokości w cm
# 6.	Weight – waga w kg
# 7.	Team – nazwa drużyny
# 8.	NOC - National Olympic Committee
# 9.	Games – rok oraz rodzaj igrzysk
# 10.	Year – rok igrzysk
# 11.	Season – Igrzyska letnia albo zimowe
# 12.	City – miejsce igrzysk
# 13.	Sport - dyscyplina
# 14.	Event – nazwa wydarzenia
# 15.	Medal - Gold, Silver, Bronze, or NA uzyskane medale. 
# 
# 

# ## Import bibliotek

# In[5]:


import pandas as pd
import numpy as np
import seaborn as sns
sns.set()
from matplotlib import pyplot as plt


# ## Import danych

# In[3]:


dane = pd.read_csv(r'C:\Users\Dom\Documents\repos\dane\athlete_events.csv')
region = pd.read_csv(r'C:\Users\Dom\Documents\repos\dane\dataset_regions.csv')


# #### Podgląd danych oraz podstawowe statystki

# In[8]:


dane.head(10)


# In[9]:


dane.describe()


# In[10]:


dane.info()


# In[11]:


region.head(10)


# In[12]:


region.info()


# In[13]:


region.isna().sum()   


# Ze względu na dużą ilość braków w kolumnie **notes** , kolumna zostanie usunięta.

# In[14]:


region = region.drop(columns=['notes'])


# ## Łączenie danych

# Dane zostają połączone za pomocą funkcji ***pd.merge***. Łącze ***dane*** i ***region*** za pomocą kolumny ***NOC***

# In[15]:


data = pd.merge(dane,region, how = 'left', on = 'NOC')


# In[48]:


data.head(10)


# # Eksploracja danych

# ## Zmienna: Sport

# #### Jakie rodzaje dyscypliny są zawarte w zestawieniu?

# In[16]:


print('Olimpiada miała miejsce w następujących dyscyplinach:', np.array(sorted(data['Sport'].unique())))


# ## Zmienna: Years

# #### W jakich latach odbywały się olimpiady?

# In[17]:


print('Olimpiady odbywały się w latach:', np.array(sorted(data['Year'].unique())))


# ## Zmienna: Region

# Spradzam ilość medali w podziale na : złoty, brązowy oraz srebrny.

# In[18]:


data['Count'] = 1   #dodaje kolumne pomocniczą do obliczeń
data.groupby(['Medal']).count()['Count']  


# ### W których krajach osiągnięto największą ilość złotych medali?

# In[19]:


gold_medals = data[(data['Medal']=='Gold')]  # Tworzę nowy DataFrame wyłącznie dla złotych medali


# In[20]:


gold_medals.groupby(['region']).count()['Count'].sort_values(ascending = False)


# #### Największa ilość medali przypada na USA

# In[21]:


gold_medals.groupby(['region']).count()['Count'].nlargest(n=5).plot(kind = 'bar')


# ### W jakich dyscyplinach w USA osiągnięto najwiecej medali?

# In[22]:


usa_sport = gold_medals.loc[gold_medals['region']=='USA']  #Tworzę nowy DataFrame


# In[23]:


usa_sport.groupby(['Sport']).count()['Count'].sort_values(ascending = False)


# In[24]:


usa_sport.groupby(['Sport']).count()['Count'].nlargest(n=5).plot(kind = 'bar',cmap='viridis',alpha=0.5)


# #### Najwięcej medali zdobyto w pływaniu, natomiast najmniej w strzelnictwie.

# ## Zmienna: Płeć

# ### Podział olimpijczyków ze względu na płeć

# In[28]:


women_number = data[data['Sex']=='F']
women_number.count()['Count']


# Wśród kobiet było **74 522** uczestniczek, natomiast ilość mężczyzn wynosiła **196 594**.

# In[27]:


import seaborn as sns
data['Sex'].unique()
data['Sex'].value_counts()
sns.countplot(data['Sex'])
plt.title('Podział płci',size=15,color='green')
plt.show()


# Wśród złotych medalistów znalazło się **3747 kobiet** oraz **9625 mężczyzn**.

# #### Na poniższym wykresie przedstawiono ilości zdobytych medali przez wszysktich olimpijczyków

# In[29]:


print(medals_all)


# In[30]:


medals_all =pd.pivot_table(data=data, values = 'Count', index='Medal', columns='Sex',aggfunc='count' )
medals_all.plot(kind = 'bar')


# #### Z powyższych wykresów mogły się wydać, że mężczyźni zdobywają więcej medali, niż kobiety. 
# #### Jednakże należy pamiętać, że jest ich blisko 2,6 razy więcej niż kobiet. Tym samym należy sprawdzić jaki jest stosunek osiąganych wszystkich medali dla każej z płci osobna.

# In[31]:


women_medals = medals_all['F'].sum()
print(women_medals)  #Ilość wszystkich medali osiąganych przez kobiety


# In[33]:


all_olympics= data.groupby('Sex').sum()['Count']


# In[34]:


women_per=round(women_medals / all_olympics.iloc[0]*100,2) #ilość medali osiągniętych przez kobiety do wszystkich kobiet
print(str(women_per)+'%')


# #### Blisko 15% startujących kobiet zdobywa medal.

# In[35]:


men_medals = medals_all['M'].sum()
print(men_medals)  #Ilość wszystkich medali osiąganych przez mężczyzn


# In[36]:


men_per=round(men_medals / all_olympics.iloc[1]*100,2) #ilość medali osiągniętych przez mężczyzn do wszystkich mężczyzn
print(str(men_per) + '%')


# #### Wśród startujących mężczyzn około 14,51% staje na podium. Tym samym, stosunek zdobytych medali wśród kobiet, był wyższy niż u mężczyzn.

# ## Z racji tego, że kobiety osiągają lepsze wyniki sporcie, sprawdźmy ile medali osiągały uczestniczki w letnich igrzyskach olimpijskich.

# In[37]:


women_summer = data[(data.Sex == 'F') & (data.Season == 'Summer')]


# In[38]:


women_summer.head(10)


# In[39]:


sns.set(style="white")
plt.figure(figsize=(20, 10))
sns.countplot(x='Year', data=women_summer)
plt.title('Ilość kobiet w danych olimpiadach letnich')


# ### W jaki sposób kształtował się wykres obu płci na przestrzeni lat?

# In[40]:


men= data[data['Sex']=='M'] #Tworze DataFrame dla mężczyzn


# Tworzę nowy DataFrame **men**, a następnie sortuję go po latach oraz zliczam ilość wystąpień mężczyzn.

# In[41]:


men_years = men.groupby('Year')['Sex'].value_counts() 


# In[207]:


men_years


# In[42]:


plt.figure(figsize=(20, 10))
men_years.loc[:,'M'].plot()
plt.title('Rozkład olimpijczyków na przetrzeni lat')


# In[43]:


women = data[data['Sex']=='F']


# In[44]:


women_years = women.groupby('Year')['Sex'].value_counts()


# In[45]:


plt.figure(figsize=(20, 10))
women_years.loc[:,'F'].plot()
plt.title('Rozkład kobiet na przestrzeni lat')


# ## Zmienna Age

# Pierwszym krokiem jest przedstawienie podstawowych statystyk dla zmiennej ***Age***

# In[46]:


data.isna().any()  #Sprawdzam czy występują braki w kolumnie Age


# Ze względu na to, że wystepują braki w zmiennej Age, usuwam wiersze dla których wiek przyjmuje wartość **NA**.

# In[47]:


data_noage = data  #Tworzę nowy DataFrame
data_noage.dropna(subset=['Age'],inplace= True) #Usuwam braki wiersze dla których Age przyjmuje NA


# In[48]:


data_noage.isna().any() # zmienna Age nie posiada braków w danych


# #### Rozkład wieku dla wszystkich uczestników wygląda w następujący sposób

# In[50]:


plt.figure(figsize=(20, 10))
plt.tight_layout()
sns.countplot(data_noage['Age'])
plt.title('Rozkład wieku olimpijczyków')


# #### Ile średnio lat mieli uczestnicy?

# In[51]:


print(' Średnia uczestników wynosi:', str(round(data_noage['Age'].mean())) + ' lat')


# #### Ile lat miał najmłodszy uczestnik olimipady?

# In[52]:


print('Najmłodszy sportowiec miał:',str(round(data.Age.min())) + ' lat')


# #### Ile lat miał najstarszy atleta?

# In[53]:



print('Najstarszy atleta miał:',str(round(data.Age.max())) + ' lat')


# In[54]:


y=np.array([data.Age.min(),data.Age.mean(),data.Age.max()])
x=['Najmlodszy','Średni wiek','Najstarszy']
plt.bar(x,y)
plt.xlabel(' ')
plt.ylabel('Wiek')
plt.show()


# #### W jaki sposób wygląda rozkład dla złotych olimpijczyków?

# In[55]:


gold_medals = data[data['Medal'] =='Gold']


# In[56]:


gold_medals = gold_medals[np.isfinite(gold_medals['Age'])] # biorę tylko te kolumny, dla których Age jest niepusty.


# In[57]:


plt.figure(figsize=(20, 10))
plt.tight_layout()
sns.countplot(gold_medals['Age'])
plt.title('Rozkład wieku złotych olimpijczyków')


# In[189]:


print(' Średnia złotych olimpijczyków wynosi:', str(round(gold_medals['Age'].mean())) + ' lat')


# #### Z powyższych wykresów wynika, że średnia wieku dla wszystkich olimpijczyków oraz dla złotych olimpijczyków wynosi 26 lat

# ### W jakich dyscyplinach, sportowcy powyżej średniej wieku zdobywali złote medale?

# In[60]:


age_mean = round(gold_medals['Age'].mean())
print(age_mean)


# In[61]:


sport_upper = gold_medals['Sport'][gold_medals['Age']>age_mean]


# In[62]:


plt.figure(figsize=(20, 10))
plt.tight_layout()
sns.countplot(sport_upper)
plt.title('Dyscypliny powyżej średniej wieku')         


# ## Zmienna: weight / height

# #### Poniżej znajdują się rozkłady dotyczące wagi oraz wysokości olimpijczyków, którzy otrzymali złote medale.

# In[63]:


gold_medals.info()


# W związku z brakiem pełnych danych, w dalszej części analizy będą rozpatrywane wyłącznie te dane, dla których są informacje dla dwóch zmiennych.

# In[64]:


gold_medals2 = gold_medals[(gold_medals['Height'].notnull()) & (gold_medals['Weight'].notnull())]


# In[201]:


gold_medals2.info() # Wszedzie posiadamy taką samą ilość


# In[65]:


plt.figure(figsize=(12, 10))
ax = sns.scatterplot(x="Weight", y="Height", data=gold_medals2)
plt.title('Wysokość oraz waga złotych olimpijczyków')


# In[66]:


wysokosc=sns.distplot(gold_medals['Height'].dropna(),color='Green',kde=True)
wysokosc.set_title('Rozkład dystrybuatny wzrostu',fontsize=16,fontweight=200)


# In[67]:


waga=sns.distplot(gold_medals['Weight'].dropna(),color='Blue',kde=True)
waga.set_title('Rozkład dystrybuanty wagi',fontsize=16,fontweight=200)


# #### Czy pomiędzy zmiennymi: wiek, waga oraz wzrost występuje korelacja?

# In[68]:


data[['Age','Height','Weight']].corr()


# Korelacja występuje pomiędzy zmiennymi **weight** oraz **height** 
